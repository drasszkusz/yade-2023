#include "ViElToCap.hpp"
#include <lib/high-precision/Constants.hpp>
#include <core/Omega.hpp>
#include <core/Scene.hpp>
#include <core/State.hpp>
#include <pkg/common/Sphere.hpp>
#include <pkg/dem/ScGeom.hpp>

namespace yade { // Cannot have #include directive inside.

YADE_PLUGIN((ViElToCapMat)(ViElToCapPhys)(Ip2_ViElToCapMat_ViElToCapMat_ViElToCapPhys)(Law2_ScGeom_ViElToCapPhys_Basic));

/* ViElToCapMat */
ViElToCapMat::~ViElToCapMat(){}

/* ViElToCapPhys */
ViElToCapPhys::~ViElToCapPhys(){}

/* Ip2_ViElToCapMat_ViElToCapMat_ViElToCapPhys */
void Ip2_ViElToCapMat_ViElToCapMat_ViElToCapPhys::go(const shared_ptr<Material>& b1, const shared_ptr<Material>& b2, const shared_ptr<Interaction>& interaction) {
  // no updates of an existing contact 
  if(interaction->phys) return;
  
  TIMING_DELTAS_START();
  TIMING_DELTAS_CHECKPOINT("setup");
  
  shared_ptr<ViElToCapPhys> phys (new ViElToCapPhys());
  Calculate_ViElToMat_ViElToMat_ViElToPhys(b1, b2, interaction, phys);
  
  ViElToCapMat* mat1 = static_cast<ViElToCapMat*>(b1.get());
  ViElToCapMat* mat2 = static_cast<ViElToCapMat*>(b2.get());
  
  TIMING_DELTAS_CHECKPOINT("collide_materials");
  
  if (mat1->Capillar and mat2->Capillar)  {
    if (mat1->Vb < 0.0 || mat2->Vb < 0.0) {
      throw runtime_error("Vb should not be negative!");
    } else {
      phys->VL1 = mat1->Vb;
      phys->VL2 = mat2->Vb;
    }
    
    if (mat1->gamma == mat2->gamma) {
      phys->gamma = mat1->gamma;
    } else {
      throw runtime_error("Gamma should be equal for both particles!");
    }
  
    if (mat1->theta == mat2->theta) {
      phys->theta = (mat1->theta*M_PI/180.0);
    } else {
      phys->theta = acos(0.5*(cos(mat1->theta*M_PI/180.0)+cos(mat2->theta*M_PI/180.0)));	// [Israelachvili2010] eq. (17.47) and (17.48)
    }
    
  
    if (mat1->dcap == mat2->dcap) {
      phys->dcap = mat1->dcap;
    } else {
      throw runtime_error("Dcap should be equal for both particles!.");
    }

    if (mat1->muvisc == mat2->muvisc) {
      phys->muvisc = mat1->muvisc;
    } else {
      throw runtime_error("Muvisc should be equal for both particles!.");
    }

    if (mat1->liq_dens == mat2->liq_dens) {
      phys->dens=std::abs(((mat1->density+mat2->density)/2.0)-mat1->liq_dens);
    } else {
      throw runtime_error("Liq_dens should be equal for both particles!.");
    }
    
    if (mat1->CapillarType == mat2->CapillarType and mat2->CapillarType != ""){
      
      if      (mat1->CapillarType == "Willett_numeric")  {phys->CapillarType = Willett_numeric;}
      else if (mat1->CapillarType == "Willett_analytic") {phys->CapillarType = Willett_analytic;}
      else if (mat1->CapillarType == "Weigert")          {phys->CapillarType = Weigert;}
      else if (mat1->CapillarType == "Rabinovich")       {phys->CapillarType = Rabinovich;}
      else if (mat1->CapillarType == "Lambert")          {phys->CapillarType = Lambert;}
      else if (mat1->CapillarType == "Soulie")           {phys->CapillarType = Soulie;}
      else if (mat1->CapillarType == "Pitois")           {phys->CapillarType = Pitois;}
      else if (mat1->CapillarType == "Richefeu")         {phys->CapillarType = Richefeu;}
      else if (mat1->CapillarType == "Mikami")           {phys->CapillarType = Mikami;}
      else if (mat1->CapillarType == "Sun")              {phys->CapillarType = Sun;}
      else                                               {phys->CapillarType = None_Capillar;}
    } else {
      throw runtime_error("CapillarType should be equal for both particles!.");
    }
    phys->Capillar=true;
  }
  #ifdef YADE_LIQMIGRATION
  phys->LiqMigrEnabled = mat1->LiqMigrEnabled && mat2->LiqMigrEnabled;
  #endif
  interaction->phys = phys;
}

/* Law2_ScGeom_ViElToCapPhys_Basic */
bool Law2_ScGeom_ViElToCapPhys_Basic::go(shared_ptr<IGeom>& _geom, shared_ptr<IPhys>& _phys, Interaction* I) {
  TIMING_DELTAS_START();
  TIMING_DELTAS_CHECKPOINT("setup");
  
  Vector3r force = Vector3r::Zero();
  
  const id_t id1 = I->getId1();
  const id_t id2 = I->getId2();
    
  const ScGeom& geom=*static_cast<ScGeom*>(_geom.get());
  Scene* scene2=Omega::instance().getScene().get();
  ViElToCapPhys& phys=*static_cast<ViElToCapPhys*>(_phys.get());
  const BodyContainer& bodies = *scene2->bodies;
  
   /*
   * This part for implementation of the capillar model.
   * All main equations are in calculateCapillarForce function. 
   * There is only the determination of critical distance between spheres, 
   * after that the liquid bridge will be broken.
   */ 
  
  TIMING_DELTAS_CHECKPOINT("create_liq_bridge");

  if (phys.Capillar and not(phys.liqBridgeCreated) and geom.penetrationDepth>=0) {
    phys.liqBridgeCreated = true;
    phys.liqBridgeActive = false;
    #ifdef YADE_LIQMIGRATION
    if (phys.LiqMigrEnabled) {
      scene2->addIntrs.push_back(I);
    }
    #endif
    Sphere* s1=dynamic_cast<Sphere*>(bodies[id1]->shape.get());
    Sphere* s2=dynamic_cast<Sphere*>(bodies[id2]->shape.get());

    // calculate liquid volume contributions [shi2008]
    Real r1, r2;
    if (s1 and s2) {
      r1 = s1->radius;
      r2 = s2->radius;
    } else {
      r1 = std::max(s1->radius,s2->radius);
      r2 = r1;
    }
    const Real Vb1 = 0.5*phys.VL1*(1 - pow(1 - r2*r2/(r1*r1 + r2*r2),0.5));	// [shi2008] eq. (4)
    const Real Vb2 = 0.5*phys.VL2*(1 - pow(1 - r1*r1/(r1*r1 + r2*r2),0.5));	// [shi2008] eq. (5)
    phys.Vb = Vb1 + Vb2;	// [shi2008] eq. (6)

    if (s1 and s2) {
      phys.R = 2 * s1->radius * s2->radius / (s1->radius + s2->radius);
    } else if (s1 and not(s2)) {
      phys.R = s1->radius;
    } else {
      phys.R = s2->radius;
    }
    if (phys.CapillarType==Richefeu) {
      if (s1 and s2) {
        phys.R1 = s1->radius;
        phys.R2 = s2->radius;
      } else if (s1 and not(s2)) {
        phys.R1 = s1->radius;
        phys.R2 = phys.R1;
      } else {
        phys.R2 = s2->radius;
        phys.R1 = phys.R2;
      }
    }
    if (phys.CapillarType==Sun) {
      if (s1 and s2) {
        phys.R1 = s1->radius;
        phys.R2 = s2->radius;
      } else if (s1 and not(s2)) {
        phys.R1 = s1->radius;
        phys.R2 = 0.0;
      } else {
        phys.R2 = s2->radius;
        phys.R1 = 0.0;
      }
    }
  }
  
  TIMING_DELTAS_CHECKPOINT("calculate_scrit");

  if (rupt_type==2 || phys.CapillarType==Pitois) {
    const State& de1 = *static_cast<State*>(bodies[id1]->state.get());
    const State& de2 = *static_cast<State*>(bodies[id2]->state.get());
        
    const Vector3r shift2 = scene2->isPeriodic ? scene2->cell->intrShiftPos(I->cellDist): Vector3r::Zero(); 
    const Vector3r shiftVel = scene2->isPeriodic ? scene2->cell->intrShiftVel(I->cellDist): Vector3r::Zero(); 
      
    const Vector3r c1x = (geom.contactPoint - de1.pos);
    const Vector3r c2x = (geom.contactPoint - de2.pos - shift2);
        
    const Vector3r relativeVelocity = (de1.vel+de1.angVel.cross(c1x)) - (de2.vel+de2.angVel.cross(c2x)) + shiftVel;
    phys.vrel=relativeVelocity.squaredNorm();
  }

  phys.sCrit = this->critDist(phys.Vb, phys.R, phys.theta, phys.gamma, phys.muvisc, phys.vrel, phys.dens);
  
  TIMING_DELTAS_CHECKPOINT("force_calculation_liquid");
  if (geom.penetrationDepth<0) {
    if (phys.liqBridgeCreated and -geom.penetrationDepth<phys.sCrit and phys.Capillar) {
      if (not(phys.liqBridgeActive)) {
        phys.liqBridgeActive=true;
        VLiqBridg += phys.Vb;
        NLiqBridg += 1;
      }
      
      const auto normalCapForceScalar = CapFunctionsPool[phys.CapillarType](geom, phys);
      Real dampCapForceScalar = 0.0;
      Vector3r normViscLiqForce = Vector3r(0,0,0);
      Vector3r shearViscLiqForce = Vector3r(0,0,0);
      
      if (phys.dcap) {
        const State& de1 = *static_cast<State*>(bodies[id1]->state.get());
        const State& de2 = *static_cast<State*>(bodies[id2]->state.get());
      
        Vector3r& shearForce = phys.shearForce;
        if (I->isFresh(scene2)) shearForce=Vector3r(0,0,0);
        shearForce = geom.rotate(shearForce);
        
        const Vector3r shift2 = scene2->isPeriodic ? scene2->cell->intrShiftPos(I->cellDist): Vector3r::Zero(); 
        const Vector3r shiftVel = scene2->isPeriodic ? scene2->cell->intrShiftVel(I->cellDist): Vector3r::Zero(); 
      
        const Vector3r c1x = (geom.contactPoint - de1.pos);
        const Vector3r c2x = (geom.contactPoint - de2.pos - shift2);
        
        const Vector3r relativeVelocity = (de1.vel+de1.angVel.cross(c1x)) - (de2.vel+de2.angVel.cross(c2x)) + shiftVel;
        const auto normalVelocity	= geom.normal.dot(relativeVelocity);
        
        dampCapForceScalar = -phys.dcap * normalVelocity;
      }

      if (Ca >= 0.0) {
	const Real gamma = phys.gamma;
	const Real muvisc = phys.muvisc;

        const State& de1 = *static_cast<State*>(bodies[id1]->state.get());
        const State& de2 = *static_cast<State*>(bodies[id2]->state.get());
      
        Vector3r& shearForce = phys.shearForce;
        if (I->isFresh(scene2)) shearForce=Vector3r(0,0,0);
        shearForce = geom.rotate(shearForce);
        
        const Vector3r shift2 = scene2->isPeriodic ? scene2->cell->intrShiftPos(I->cellDist): Vector3r::Zero(); 
        const Vector3r shiftVel = scene2->isPeriodic ? scene2->cell->intrShiftVel(I->cellDist): Vector3r::Zero(); 
      
        const Vector3r c1x = (geom.contactPoint - de1.pos);
        const Vector3r c2x = (geom.contactPoint - de2.pos - shift2);
        
        const Vector3r relativeVelocity = (de1.vel+de1.angVel.cross(c1x)) - (de2.vel+de2.angVel.cross(c2x)) + shiftVel;
        const auto normalVelocity	= geom.normal.dot(relativeVelocity);
	const Vector3r shearVelocity	= relativeVelocity-normalVelocity*geom.normal;

	const Real Ca_calculated = muvisc * relativeVelocity.squaredNorm() / gamma;

	if (Ca <= Ca_calculated || Ca == 0.0) {
	  Real s = -geom.penetrationDepth;
	  const Real R = phys.R;
	  if (s == 0.0) s = phys.R/100.0;	// [Anand] after eq. (7)

	  const Real normConst = 6.0*Mathr::PI*muvisc*R*R/s;	// [Lian1998]
	  const Real shearConst = ((8.0/15.0)*log(R/s)+0.9588)*6*Mathr::PI*muvisc*R;	// [Lian1998]
          normViscLiqForce = normConst*(normalVelocity*geom.normal);	// [Lian1998]
	  shearViscLiqForce = shearConst*shearVelocity;	// [Lian1998]

	}
      }
      
      phys.normalForce = -(normalCapForceScalar + dampCapForceScalar)*geom.normal + normViscLiqForce;
      phys.shearForce = shearViscLiqForce;
      
      if (I->isActive) {
        addForce (id1,-phys.normalForce - phys.shearForce,scene2);
        addForce (id2, phys.normalForce + phys.shearForce,scene2);
      };
      return true;
    } else {
      if (phys.liqBridgeActive) {
        VLiqBridg -= phys.Vb;
        NLiqBridg -= 1;
      }
      #ifdef YADE_LIQMIGRATION
        if (phys.Vb > 0.0 and ((phys.Vf1+phys.Vf2) == 0.0)) {
          phys.Vf1 = phys.Vb/2.0;
          phys.Vf2 = phys.Vb/2.0;
        }
        const std::pair<id_t, Real > B1 = {id1, phys.Vf1};
        const std::pair<id_t, Real > B2 = {id2, phys.Vf2};
        scene2->delIntrs.push_back(B1);
        scene2->delIntrs.push_back(B2);
      #endif
      return false;
    };
  };
  
  if (phys.liqBridgeActive) {
    phys.liqBridgeActive=false;
    VLiqBridg -= phys.Vb;
    NLiqBridg -= 1;
  }
  
  if (I->isActive) {
    
    Vector3r torque1 = Vector3r::Zero();
    Vector3r torque2 = Vector3r::Zero();
    
    TIMING_DELTAS_CHECKPOINT("force_calculation_penetr");
    if (computeForceTorqueViElTo(_geom, _phys, I, force, torque1, torque2)) {
      addForce (id1,-force,scene2);
      addForce (id2, force,scene2);
      addTorque(id1, torque1,scene2);
      addTorque(id2, torque2,scene2);
      return true;
    } else {
      return false;
    }
  }
  return true;
}

Real Law2_ScGeom_ViElToCapPhys_Basic::critDist(const Real& Vb, const Real& R, const Real& Theta, const Real& Gamma, const Real& Muvisc, const Real& Vrel, const Real& Dens) {
  if (rupt_type==1) { //Willet2000
      const Real Vstar = Vb/(R*R*R);
      const Real Sstar = (1+0.5*Theta)*(pow(Vstar,1/3.0) + 0.1*pow(Vstar,2.0/3.0));   // [Willett2000], equation (15), use the full-length e.g 2*Sc
      const Real critDist = Sstar*R;
      return critDist;
  } else if (rupt_type==2) { //Pitois2001
      const Real Vstar = Vb/(R*R*R);
      const Real Ca_P = (Muvisc*Vrel)/Gamma;
      const Real Sstar = (1+0.5*Theta) + (1+pow(Ca_P,1.0/2.0))*pow(Vstar,1/3.0);   // [Pitois2001] eq. (19)
      const Real critDist = Sstar*R;
      return critDist;
  } else if (rupt_type==3) {  //Adams2002
      const Real Vstar = Vb/(R*R*R);
      const Real Bo = ((Dens*9.81*Vb)/(R*Gamma)); //Lievano2017 eq. (7)
      const Real Sstar = (1-0.48*Vstar*Bo)*pow(Vstar,1/3.0);   // [Adams2002] eq. (4)
      const Real critDist = Sstar*R;
      return critDist;
  } else if (rupt_type==4) {  // Mikami1998
      const Real Vstar = Vb/(R*R*R);
      const Real Sstar = (0.62*Theta+0.99)*pow(Vstar,0.34);   // [Mikami1998] eq. (24)
      const Real critDist = Sstar*R;
      return critDist;
  } else {
    throw runtime_error("Error! Value of rupt_type parameter is wrong! rupt_type=1 for Willet2000; =2 for Pitois2001 and =3 for Adams2002!");
  }

}

//=========================================================================================
//======================Capillary bridge models============================================
//=========================================================================================

Real Law2_ScGeom_ViElToCapPhys_Basic::Willett_numeric_f(const ScGeom& geom, ViElToCapPhys& phys) {
  /* 
   * Capillar model from [Willett2000]
   */ 
  
  const Real R = phys.R;
  const Real s = -geom.penetrationDepth;
  const Real Vb = phys.Vb;
  
  const Real VbS = Vb/(R*R*R);
  const Real Th1 = phys.theta;
  const Real Th2 = phys.theta*phys.theta;
  const Real Gamma = phys.gamma;
  
  /*
   * [Willett2000], equations in Attachment
  */
  const Real f1 = (-0.44507 + 0.050832*Th1 - 1.1466*Th2) + 
            (-0.1119 - 0.000411*Th1 - 0.1490*Th2) * log(VbS) +
            (-0.012101 - 0.0036456*Th1 - 0.01255*Th2) *log(VbS)*log(VbS) +
            (-0.0005 - 0.0003505*Th1 - 0.00029076*Th2) *log(VbS)*log(VbS)*log(VbS);
  
  const Real f2 = (1.9222 - 0.57473*Th1 - 1.2918*Th2) +
            (-0.0668 - 0.1201*Th1 - 0.22574*Th2) * log(VbS) +
            (-0.0013375 - 0.0068988*Th1 - 0.01137*Th2) *log(VbS)*log(VbS);
            
  const Real f3 = (1.268 - 0.01396*Th1 - 0.23566*Th2) +
            (0.198 + 0.092*Th1 - 0.06418*Th2) * log(VbS) +
            (0.02232 + 0.02238*Th1 - 0.009853*Th2) *log(VbS)*log(VbS) +
            (0.0008585 + 0.001318*Th1 - 0.00053*Th2) *log(VbS)*log(VbS)*log(VbS);
  
  const Real f4 = (-0.010703 + 0.073776*Th1 - 0.34742*Th2) +
            (0.03345 + 0.04543*Th1 - 0.09056*Th2) * log(VbS) +
            (0.0018574 + 0.004456*Th1 - 0.006257*Th2) *log(VbS)*log(VbS);

  const Real sPl = (s/2.0)/sqrt(Vb/R);
  
  const Real lnFS = f1 - f2*exp(f3*log(sPl) + f4*log(sPl)*log(sPl));
  const Real FS = exp(lnFS);
  
  const Real fC = FS * 2.0 * M_PI* R * Gamma;
  return fC;
}

Real Law2_ScGeom_ViElToCapPhys_Basic::Willett_analytic_f(const ScGeom& geom, ViElToCapPhys& phys) {
  /* 
   * Capillar model from Willet [Willett2000] (analytical solution), but 
   * used also in the work of Herminghaus [Herminghaus2005]
   */
   
  const Real R = phys.R;
  const Real Gamma = phys.gamma;
  const Real s = -geom.penetrationDepth;
  const Real Vb = phys.Vb;
          
  /*
  Real sPl = s/sqrt(Vb/R);                                                            // [Herminghaus2005], equation (sentence between (7) and (8))
  fC = 2.0 * M_PI* R * Gamma * cos(phys.theta)/(1 + 1.05*sPl + 2.5 *sPl * sPl);       // [Herminghaus2005], equation (7)
  */ 
  
  const Real sPl = (s/2.0)/sqrt(Vb/R);                                                      // [Willett2000], equation (sentence after (11)), s - half-separation, so s*2.0
  const Real f_star = cos(phys.theta)/(1 + 2.1*sPl + 10.0 * pow(sPl, 2.0));                 // [Willett2000], equation (12)
  const Real fC = f_star * (2*M_PI*R*Gamma);                                                     // [Willett2000], equation (13), against F
  
  return fC;
}

Real Law2_ScGeom_ViElToCapPhys_Basic::Weigert_f(const ScGeom& geom, ViElToCapPhys& phys) {
 /*
  *  Capillar model from [Weigert1999]
  */
  const Real R = phys.R;
  const Real a = -geom.penetrationDepth;
  const Real Caa = (1.0 + 6.0*a/(R*2.0));                                                          // [Weigert1999], equation (16)
  const Real Ct = (1.0 + 1.1*sin(phys.theta));                                                    // [Weigert1999], equation (17)
  
  /*
  Real Eps = 0.36;                                                                          // Porosity
  Real fi = phys.Vb/(2.0*M_PI/6.0*pow(R*2.0,3.));                                           // [Weigert1999], equation (13)
  Real S = M_PI*(1-Eps)/(pow(Eps, 2.0))*fi;                                                 // [Weigert1999], equation (14)
  Real beta = asin(pow(((S/0.36)*(pow(Eps, 2.0)/(1-Eps))*(1.0/Caa)*(1.0/Ct)), 1.0/4.0));     // [Weigert1999], equation (19)
  */
  
  const Real beta = asin(pow(phys.Vb/(0.12*Caa*Ct*pow(2.0*R, 3.0)), 1.0/4.0));                     // [Weigert1999], equation (15), against Vb
  
  const Real r1 = (2.0*R*(1-cos(beta)) + a)/(2.0*cos(beta+phys.theta));                           // [Weigert1999], equation (5)
  const Real r2 = R*sin(beta) + r1*(sin(beta+phys.theta)-1);                                      // [Weigert1999], equation (6)
  const Real Pk = phys.gamma*(1/r1 - 1/r2);                                                       // [Weigert1999], equation (22),
                                                                                                  // see also a sentence over the equation
                                                                                                  // "R1 was taken as positive and R2 was taken as negative"

  //fC = M_PI*2.0*R*phys.gamma/(1+tan(0.5*beta));                                           // [Weigert1999], equation (23), [Fisher]
  
  const Real fC = M_PI/4.0*pow((2.0*R),2.0)*pow(sin(beta),2.0)*Pk +
                  phys.gamma*M_PI*2.0*R*sin(beta)*sin(beta+phys.theta);                     // [Weigert1999], equation (21)
  
  return fC;
}

Real Law2_ScGeom_ViElToCapPhys_Basic::Rabinovich_f(const ScGeom& geom, ViElToCapPhys& phys) {
  /* 
   * Capillar model from Rabinovich [Rabinov2005]
   *
   * This formulation from Rabinovich has been later verified and corrected
   * by Lambert [Lambert2008]. So we can calculate both formulations
   * 
   */
     
  const Real R = phys.R;
  const Real Gamma = phys.gamma;
  const Real H = -geom.penetrationDepth;
  const Real V = phys.Vb;
  
  Real fC = 0.0;
  Real dsp = 0.0;
  
  if (H!=0.0) {
    dsp = H/2.0*(-1.0 + sqrt(1.0 + 2.0*V/(M_PI*R*H*H)));                            // [Rabinov2005], equation (20)
    fC = -(2*M_PI*R*Gamma*cos(phys.theta))/(1+(H/(2*dsp)));                         // [Lambert2008], equation (65), taken from [Rabinov2005]
    const Real alpha = sqrt(H/R*(-1+ sqrt(1 + 2.0*V/(M_PI*R*H*H))));              // [Rabinov2005], equation (A3)
    fC -= 2*M_PI*R*Gamma*sin(alpha)*sin(phys.theta + alpha);                      // [Rabinov2005], equation (19)
  } else {
    fC = -(2*M_PI*R*Gamma*cos(phys.theta));
    const Real alpha = 0.0;
    fC -= 2*M_PI*R*Gamma*sin(alpha)*sin(phys.theta + alpha);                      // [Rabinov2005], equation (19)
  }
    
  fC *=-1;
  return fC;
}

Real Law2_ScGeom_ViElToCapPhys_Basic::Lambert_f(const ScGeom& geom, ViElToCapPhys& phys) {
  /* 
   * Capillar model from Rabinovich [Rabinov2005]
   *
   * This formulation from Rabinovich has been later verified and corrected
   * by Lambert [Lambert2008]. So we can calculate both formulations
   * 
   */
     
  const Real R = phys.R;
  const Real Gamma = phys.gamma;
  const Real H = -geom.penetrationDepth;
  const Real V = phys.Vb;
  
  Real fC = 0.0;
  Real dsp = 0.0;
  
  if (H!=0.0) {
    dsp = H/2.0*(-1.0 + sqrt(1.0 + 2.0*V/(M_PI*R*H*H)));                            // [Rabinov2005], equation (20)
    fC = -(2*M_PI*R*Gamma*cos(phys.theta))/(1+(H/(2*dsp)));                         // [Lambert2008], equation (65), taken from [Rabinov2005]
  } else {
    fC = -(2*M_PI*R*Gamma*cos(phys.theta));
  }
  
  fC *=-1;
  return fC;
}

Real Law2_ScGeom_ViElToCapPhys_Basic::Soulie_f(const ScGeom& geom, ViElToCapPhys& phys) {
  /* 
   * Capillar model from Soulie [Soulie2006]
   *
   * !!! In this implementation the radiis of particles are taken equal
   * to get the symmetric forces.
   * 
   * Please, use this model only for testing purposes.
   * 
   */
  
  const Real R = phys.R;
  const Real Gamma = phys.gamma;
  const Real D = -geom.penetrationDepth;
  const Real V = phys.Vb;
  const Real Theta = phys.theta;
  
  const Real a = -1.1*pow((V/(R*R*R)), -0.53);
  const Real b = (-0.148*log(V/(R*R*R)) - 0.96)*Theta*Theta -0.0082*log(V/(R*R*R)) + 0.48;
  const Real c = 0.0018*log(V/(R*R*R)) + 0.078;
  
  const Real fC = Mathr::PI*Gamma*sqrt(R*R)*(c+exp(a*D/R+b));
  
  return fC;
}

Real Law2_ScGeom_ViElToCapPhys_Basic::Pitois_f(const ScGeom& geom, ViElToCapPhys& phys) {
  /* 
   * Capillar model from Pitois [Pitois2001]
   * 
   */
  
  const Real R = phys.R;
  const Real Gamma = phys.gamma;
  const Real V = phys.Vb;
  const Real Theta = phys.theta;
  const Real Muvisc = phys.muvisc;
  const Real Vrel = phys.vrel;
  Real s = -geom.penetrationDepth;
  
  if (s == 0.0) s = 1.0E-6;	// [Tamrakar2019] after eq. (14)

  const Real Ca_p = (Muvisc*Vrel)/Gamma;  // [Pitois2001] after eq. (11)
  const Real a = 1.0-pow((1.0+2*(V/(R*R*R))/(Mathr::PI*pow(s/R,2.0))),-0.5);  // [Pitois2001] eq.
  
  const Real fC = Mathr::PI*R*Gamma*(2.0*cos(Theta)*a+1.5*Ca_p*a*a/(s/R));  // [Pitois2001] eq. (14)
  
  return fC;
}

Real Law2_ScGeom_ViElToCapPhys_Basic::Richefeu_f(const ScGeom& geom, ViElToCapPhys& phys) {
  /* 
   * Capillar model from Richefeu [Richefeu2008]
   * 
   */
  
  const Real R1 = phys.R1;
  const Real R2 = phys.R2;
  const Real Reff = pow(R1*R2,0.5);  // [Richefeu2008] under eq. (5)
  const Real Gamma = phys.gamma;
  const Real s = -geom.penetrationDepth;
  const Real V = phys.Vb;
  const Real Theta = phys.theta;
  const Real a1 = Mathr::PI*2.0*Gamma*cos(Theta);  // [Richefeu2008] eq. (5)
  const Real a2 = 0.9*pow(2.0,-0.5)*pow(V,0.5)*pow((std::max(R1/R2,R2/R1)),-0.5)*pow((1.0/R1)+(1.0/R2),0.5);  // [Richefeu2008] eq.(11)

  Real fC = 0.0;
  if (s<0.0) {
    fC = -a1*Reff;  // [Richefeu2008] eq. (10)
  } else {
    fC = -a1*Reff*exp(-s/a2);  // [Richefeu2008] eq. (10)
  }

  fC *=-1;
  return fC;
}

Real Law2_ScGeom_ViElToCapPhys_Basic::Mikami_f(const ScGeom& geom, ViElToCapPhys& phys) {
  /* 
   * Capillar model from Mikami [Mikami1998]
   * 
   */
  
  const Real R = phys.R;
  const Real Gamma = phys.gamma;
  const Real s = -geom.penetrationDepth;
  const Real V = phys.Vb;
  const Real Theta = phys.theta;
  const Real Vstar=V/(R*R*R);
  const Real Sstar=s/R;

  const Real A = -1.1*pow(Vstar, -0.53);  // [Mikami1998] eq. (23-1)
  const Real B = (-0.34*log(Vstar)-0.96)*pow(Theta, 2.0)-0.019*log(Vstar)+0.48;  // [Mikami1998] eq. (23-1)
  const Real C = 0.0042*log(Vstar)+0.078;  // [Mikami1998] eq. (23-1)

  const Real fC = Mathr::PI*R*Gamma*(exp(A*Sstar+B)+C);  // [Mikami1998] eq. (22)

  return fC;
}

Real Law2_ScGeom_ViElToCapPhys_Basic::Sun_f(const ScGeom& geom, ViElToCapPhys& phys) {
  /* 
   * Capillar model from Sun's toroidal approximation [Sun2018]
   * 
   */
  const Real Vb = phys.Vb;
  const Real s = -geom.penetrationDepth;
  const Real Gamma = phys.gamma;

  Real R1, R2, Theta1, Theta2;
  if (phys.R1 <= phys.R2) {
    if (phys.R1 != 0.0) {
      R1 = phys.R1;
      R2 = phys.R2;
      Theta1 = phys.theta1*(Mathr::PI/180.0);
      Theta2 = phys.theta2*(Mathr::PI/180.0);
    }
    else {
      R1 = phys.R2;
      R2 = 0.0;
      Theta1 = phys.theta2*(Mathr::PI/180.0);
      Theta2 = phys.theta1*(Mathr::PI/180.0);
    }
  }
  else {
    if (phys.R2 != 0.0) {
      R1 = phys.R2;
      R2 = phys.R1;
      Theta1 = phys.theta2*(Mathr::PI/180.0);
      Theta2 = phys.theta1*(Mathr::PI/180.0);
    }
    else {
      R1 = phys.R1;
      R2 = 0.0;
      Theta1 = phys.theta1*(Mathr::PI/180.0);
      Theta2 = phys.theta2*(Mathr::PI/180.0);
    }
  }

  Real fC = 0.0;
  //  calculate alpha1 guess
  Real coef = 0.0;
  if (R2 > 0.0) {
    coef = 1.0 + R1/R2;
  }
  else {
    coef = 1.0;
  }
  const Real Sstar = s/R1;
  const Real Vstar = Vb/(Mathr::PI*R1*R1*R1);
  const Real a1 = (-Sstar+pow(Sstar*Sstar+coef*Vstar,0.5)/(coef/2.0));  // [sun2018] eq. (35)

  Real alpha1 = pow(a1,0.5);  // [sun2018] eq. (35)

  // Check if alpha1 was converged or not (then use Newton's method)
  int conv = 0;
  Real fun = calc_func(R1, R2, Vb, s, Theta1, Theta2, alpha1);
  //  check initial converge
  if (std::abs(fun) <= 1.0E-6) {
    conv = 1;
  } else {
    //  newton iterations
    int iter = 0;
    bool exitg1 = false;
    while ((!exitg1) && (iter < 25)) {
      //  finite difference
      Real fp = calc_func(R1, R2, Vb, s, Theta1, Theta2, alpha1 + 1.0E-6);
      Real fm = calc_func(R1, R2, Vb, s, Theta1, Theta2, alpha1 - 1.0E-6);
      Real df = (fp - fm) / 2.0E-6;

      if (df == 0.0) {
        alpha1 += 1.0E-6;
        iter++;
      } else {
        fun = calc_func(R1, R2, Vb, s, Theta1, Theta2, alpha1);
        Real xincr = -fun / df;
        alpha1 += xincr;
        fun = calc_func(R1, R2, Vb, s, Theta1, Theta2, alpha1);

        //  check convergence
        if (std::abs(fun) <= 1.0E-6) {
          conv = 1;
          exitg1 = true;
        } else {
          iter++;
        }
      }
    }
  }

  Real alpha2, rho1, rho2, b1, b2;
  //  bridge solution found
  if (conv != 0) {
    //  calculate right embracing angle
    alpha2 = calc_alpha2(R1,R2,s,Theta1,Theta2,alpha1);

    //  calculate curvature radii
    const Real at1 = alpha1 + Theta1;
    const Real at2 = alpha2 + Theta2;

    rho1 = (R1*(1.0-cos(alpha1)) + R2*(1.0-cos(alpha2)) + s) / (cos(at1)+cos(at2));  //[sun2018] eq. (24)
    rho2 = R1*sin(alpha1) - rho1*(1.0-sin(at1));  //[sun2018] eq. (25)

    //  check the validity of neck
    const Real xneck = R1*cos(alpha1) + rho1*cos(alpha1+Theta1);  //[sun2018] eq. (37)
    const Real necklb = R1*cos(alpha1);  //[sun2018] eq. (38)
    const Real neckub = R1 + s + R2 -R2*cos(alpha2);  //[sun2018] eq. (38)

    if ((xneck < necklb) || (xneck > neckub)) {
      //  neck falls inside solid
      //  apply the correction
      b1 = R1*sin(alpha1);  //[sun2018] eq. (40)
      if (R2 > 0.0) {
        b2 = R2*sin(alpha2);  //[sun2018] eq. (43)
      } else {
        b2 = b1 + rho1*sin(alpha1+Theta1) - rho1*sin(Theta2);  //[sun2018] eq. (40)
      }

      const Real rho2a = b1 / sin(alpha1+Theta1);  //[sun2018] eq. (39)
      const Real rho2b = b2 / sin(alpha2+Theta2);  //[sun2018] eq. (39)
      rho2 = 0.5*(rho2a+rho2b);  //[sun2018] eq. (39)

      //  pressure
      const Real pres = Gamma * (1.0 / rho2 - 1.0 / rho1);  //[sun2018] eq. (1) and (2)

      //  force
      const Real fC1 = -(pres*Mathr::PI*b1*b1) + 2.0*Gamma*Mathr::PI*b1*sin(alpha1 + Theta1);  //[sun2018] eq. (41)
      const Real fC2 = -(pres*Mathr::PI*b2*b2) + 2.0*Gamma*Mathr::PI*b2*sin(alpha2 + Theta2);  //[sun2018] eq. (41)
      
      fC = 0.5*(fC1 + fC2);  //[sun2018] eq. (42)

    } else {
      //  neck exists
      //  gorge method
      fC = Mathr::PI*Gamma*rho2*(1.0 + rho2 / rho1);  //[sun2018] eq. (36)
    }
  }
  else {
    cerr<<"Warning in ViElToCap.cpp : Newton-Raphson algorithm did not converged, alpha1="<<alpha1<<endl;
  }
  return fC;
}

Real Law2_ScGeom_ViElToCapPhys_Basic::None_f(const ScGeom& /*geom*/, ViElToCapPhys& /*phys*/) { return 0; }

//calculate function [sun2018] eq. (32)
Real calc_func(const Real& R1, const Real& R2, const Real& Vb, const Real& s, const Real& Theta1, const Real& Theta2, const Real& alpha1) {
  Real alpha2 = calc_alpha2(R1,R2,s,Theta1,Theta2,alpha1);
  Real at1 = alpha1 + Theta1;
  Real at2 = alpha2 + Theta2;
  Real ca1 = cos(alpha1);
  Real ca2 = cos(alpha2);
  Real cat1 = cos(at1);
  Real cat2 = cos(at2);
  Real sat1 = sin(at1);
  Real sat2 = sin(at2);

  //calculate curvature radii [sun2018]
  Real rho1 = (R1*(1.0-cos(alpha1)) + R2*(1.0-cos(alpha2)) + s) / (cos(at1)+cos(at2));  //[sun2018] eq. (24)
  Real rho2 = R1*sin(alpha1) - rho1*(1.0-sin(at1));  //[sun2018] eq. (25)

  //calculate volume
  Real vrot = (pow(rho1+rho2,2.0)*rho1 + rho1*rho1*rho1) * (cat1+cat2);  //[sun2018] eq. (30)
  vrot += (rho1 + rho2)*rho1*rho1*(at1 + at2 - Mathr::PI);  //[sun2018] eq. (30)
  vrot -= (rho1 + rho2)*rho1*rho1*(sat1*cat1 + sat2*cat2);  //[sun2018] eq. (30)
  vrot -= 1.0/3.0*pow(rho1,3.0)*(pow(cat1,3.0)+pow(cat2,3.0));  //[sun2018] eq. (30)
  vrot *= Mathr::PI;  //[sun2018] eq. (30)

  Real vcap1 = (Mathr::PI/3.0)*pow(R1,3.0)*(2.0 - 3.0*ca1 + pow(ca1,3.0));  //[sun2018] eq. (19)
  Real vcap2 = (Mathr::PI/3.0)*pow(R2,3.0)*(2.0 - 3.0*ca2 + pow(ca2,3.0));  //[sun2018] eq. (19)

  Real vol = (vrot - vcap1 - vcap2);  //[sun2018] eq. (31)

  return (vol/Vb - 1.0);
}

// calculate alpha2 [sun2018]
Real calc_alpha2(const Real& R1, const Real& R2, const Real& s, const Real& Theta1, const Real& Theta2, const Real& alpha1) {
  Real alpha2;
  if (R2 > 0.0) {
    Real A = tan(alpha1/2.0);
    Real C = tan((Theta1 - Theta2)/2.0);
    alpha2 = 2.0*atan2((R1*A + s*A/2.0 + s*C/2.0),(R2 + s/2.0 -R1*A*C - R2*A*C - s*A*C/2.0));   //[sun2018] eq. (28)
  }
  else {
    alpha2 = 0.0;
  }
  return alpha2;
}

#ifdef YADE_LIQMIGRATION
YADE_PLUGIN((LiqControl));
void LiqControl::action(){
  // This function implements liquid migration model, introduced here [Mani2013]
  mapBodyInt bI;
  mapBodyInt bodyNeedUpdate;
  
  // Calculate, how much new contacts will be at each body
  for (unsigned int i=0; i<scene->addIntrs.size(); i++) {
    shared_ptr<Body> b1 = Body::byId(scene->addIntrs[i]->getId1(),scene);
    shared_ptr<Body> b2 = Body::byId(scene->addIntrs[i]->getId2(),scene);
    
    if(not(mask!=0 && ((b1->groupMask & b2->groupMask & mask)==0))) {
      addBodyMapInt( bI, scene->addIntrs[i]->getId1() );
      addBodyMapInt( bI, scene->addIntrs[i]->getId2() );
    }
  }
  
  // Update volume water at each deleted interaction for each body
  for (unsigned int i=0; i<scene->delIntrs.size(); i++) {
    shared_ptr<Body> b = Body::byId(scene->delIntrs[i].first,scene);
    b->state->Vf += scene->delIntrs[i].second;
    addBodyMapInt(bodyNeedUpdate, scene->delIntrs[i].first);
    liqVolRup += scene->delIntrs[i].second;
  }
  scene->delIntrs.clear();
  
  // Update volume bridge at each new added interaction
  mapBodyReal bodyUpdateLiquid;
  for (unsigned int i=0; i<scene->addIntrs.size(); i++) {
    shared_ptr<Body> b1 = Body::byId(scene->addIntrs[i]->getId1(),scene);
    shared_ptr<Body> b2 = Body::byId(scene->addIntrs[i]->getId2(),scene);
    
    const id_t id1 = b1->id;
    const id_t id2 = b2->id;
    
    ViElToCapPhys* Vb=dynamic_cast<ViElToCapPhys*>(scene->addIntrs[i]->phys.get());
     
    if(mask!=0 && ((b1->groupMask & b2->groupMask & mask)==0)) {
      Vb->Vb = 0.0;
      Vb->Vf1 = 0.0;
      Vb->Vf2 = 0.0;
      Vb->Vmax = 0.0;
    } else {
      const Real Vmax = vMax(b1, b2);
      Vb->Vmax = Vmax;
      
      Real Vf1 = 0.0;
      Real Vf2 = 0.0;
      
      if ((b1->state->Vmin)<b1->state->Vf) { Vf1 = (b1->state->Vf - b1->state->Vmin)/bI[id1]; }
      if ((b2->state->Vmin)<b2->state->Vf) { Vf2 = (b2->state->Vf - b2->state->Vmin)/bI[id2]; }
      
      Real Vrup = Vf1+Vf2;
      
      if (Vrup > Vmax) {
        Vf1 *= Vmax/Vrup;
        Vf2 *= Vmax/Vrup;
        Vrup = Vf1 + Vf2;
      }
      
      liqVolShr += Vrup;
      addBodyMapReal(bodyUpdateLiquid, id1, -Vf1);
      addBodyMapReal(bodyUpdateLiquid, id2, -Vf2);
      
      Vb->Vb = Vrup;
      if (particleconserve) {
        Vb->Vf1 = Vf1;
        Vb->Vf2 = Vf2;
      } else {
        Vb->Vf1 = Vrup/2.0;
        Vb->Vf2 = Vrup/2.0;
      }
    }
  }
  
  scene->addIntrs.clear();
  
  // Update water volume in body
  for (mapBodyReal::const_iterator it = bodyUpdateLiquid.begin(); it != bodyUpdateLiquid.end(); ++it) {
    Body::byId(it->first)->state->Vf += it->second;
  }
  
  // Update contacts around body
  for (mapBodyInt::const_iterator it = bodyNeedUpdate.begin(); it != bodyNeedUpdate.end(); ++it) {
    updateLiquid(Body::byId(it->first));
  }
}

void LiqControl::updateLiquid(shared_ptr<Body> b){
  if (b->state->Vf<=b->state->Vmin) {
    return;
  } else {
    // How much liquid can body share
    const Real LiqCanBeShared = b->state->Vf - b->state->Vmin;
    
    // Check how much liquid can accept contacts 
    Real LiqContactsAccept = 0.0;
    unsigned int contactN = 0;
    for(Body::MapId2IntrT::iterator it=b->intrs.begin(),end=b->intrs.end(); it!=end; ++it) {
      if(!((*it).second) or !(((*it).second)->isReal()))  continue;
      ViElToCapPhys* physT=dynamic_cast<ViElToCapPhys*>(((*it).second)->phys.get());
      if ((physT->Vb < physT->Vmax) and (physT->Vmax > 0)) {
        LiqContactsAccept+=physT->Vmax-physT->Vb;
        contactN++;
      }
    }
    
    if (contactN>0) {
      //There are some contacts, which can be filled
      Real FillLevel = 0.0;
      if (LiqContactsAccept > LiqCanBeShared) {   // Share all available liquid from body to contacts
        const Real LiquidWillBeShared = b->state->Vf - b->state->Vmin;
        b->state->Vf = b->state->Vmin;
        FillLevel = LiquidWillBeShared/LiqContactsAccept;
      } else {                                    // Not all available liquid from body can be shared
        b->state->Vf -= LiqContactsAccept;
        FillLevel = 1.0;
      }
      
      for(Body::MapId2IntrT::iterator it=b->intrs.begin(),end=b->intrs.end(); it!=end; ++it) {
        if(!((*it).second) or !(((*it).second)->isReal()))  continue;
        ViElToCapPhys* physT=dynamic_cast<ViElToCapPhys*>(((*it).second)->phys.get());
        if ((physT->Vb < physT->Vmax) and (physT->Vmax > 0)) {
          const Real addVolLiq =  (physT->Vmax - physT->Vb)*FillLevel;
          liqVolShr += addVolLiq;
          physT->Vb += addVolLiq;
          if (particleconserve) {
            if (((*it).second)->getId1() == (*it).first) {
              physT->Vf2+=addVolLiq;
            } else if (((*it).second)->getId2() == (*it).first) {
              physT->Vf1+=addVolLiq;
            }
          } else {
            physT->Vf1+=addVolLiq/2.0;
            physT->Vf2+=addVolLiq/2.0;
          }
        }
      }
      return;
    } else {
      return;
    }
  }
}

void LiqControl::addBodyMapInt( mapBodyInt &  m, Body::id_t b  ){
  mapBodyInt::const_iterator got;
  got = m.find (b);
  if ( got == m.end() ) {
    m.insert (mapBodyInt::value_type(b,1));
  } else {
    m[b] += 1;
  }
}

void LiqControl::addBodyMapReal( mapBodyReal & m, Body::id_t b, Real addV ) {
  mapBodyReal::const_iterator got;
  got = m.find (b);
  if ( got == m.end() ) {
    m.insert (mapBodyReal::value_type(b, addV));
  } else {
    m[b] += addV;
  }
}

Real LiqControl::vMax(shared_ptr<Body> const b1, shared_ptr<Body> const b2) {
  Sphere* s1=dynamic_cast<Sphere*>(b1->shape.get());
  Sphere* s2=dynamic_cast<Sphere*>(b2->shape.get());
  Real minR = 0.0;
  if (s1 and s2) {
    minR = ::stdmin (s1->radius, s2->radius);
  } else if (s1 and not(s2)) {
    minR = s1->radius;
  } else {
    minR = s2->radius;
  }
  return vMaxCoef*minR*minR*minR;
}

Real liqVolIterBody (shared_ptr<Body> b) {
  Real LiqVol = 0.0;
  if (!b) {
    return LiqVol;
  } else {
    for(Body::MapId2IntrT::iterator it=b->intrs.begin(),end=b->intrs.end(); it!=end; ++it) {
      if(!((*it).second) or !(((*it).second)->isReal()))  continue;
      ViElToCapPhys* physT=dynamic_cast<ViElToCapPhys*>(((*it).second)->phys.get());
      if (physT and physT->Vb and physT->Vb>0) {
        if (((*it).second)->id1 == b->id) {
          if (physT->Vf1 > 0 or physT->Vf2 > 0) {
            LiqVol += physT->Vf1;
          } else {
            LiqVol += physT->Vb/2.0;
          }
        } else {
          if (physT->Vf1 > 0 or physT->Vf2 > 0) {
            LiqVol += physT->Vf2;
          } else {
            LiqVol += physT->Vb/2.0;
          }
        }
      }
    }
    return LiqVol;
  }
}

Real LiqControl::liqVolBody (id_t id) const {
  Scene* scene2=Omega::instance().getScene().get();
  const BodyContainer& bodies = *scene2->bodies;
  if (bodies[id]) {
    if (bodies[id]->state->Vf > 0) {
      return bodies[id]->state->Vf + liqVolIterBody(bodies[id]);
    } else {
      return liqVolIterBody(bodies[id]);
    }
  }
  else return -1;
}

Real LiqControl::totalLiqVol(int mask=0) const{
  Scene* scene=Omega::instance().getScene().get();
  Real totalLiqVol = 0.0;
  FOREACH(const shared_ptr<Body>& b, *scene2->bodies){
    if((mask>0 && (b->groupMask & mask)==0) or (!b)) continue;
    totalLiqVol += liqVolIterBody(b);
    if (b->state->Vf > 0) {totalLiqVol +=b->state->Vf;}
  }
  return totalLiqVol;
}

bool LiqControl::addLiqInter(id_t id1, id_t id2, Real liq) {
  if (id1==id2 or liq<=0) return false;
  
  Scene* scene2=Omega::instance().getScene().get();
  shared_ptr<InteractionContainer>& intrs=scene2->interactions;
  const shared_ptr<Interaction>& I=intrs->find(id1,id2);
  if (I->isReal()) {
    ViElToCapPhys* physT=dynamic_cast<ViElToCapPhys*>(I->phys.get());
    if (physT and physT->Vb <= physT->Vmax and liq <= (physT->Vmax - physT->Vb)) {
      physT->Vb += liq;
      physT->Vf1 += liq/2.0;
      physT->Vf2 += liq/2.0;
      return true;
    }
  }
  return false;
}
#endif

} // namespace yade

