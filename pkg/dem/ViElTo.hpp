// 2019 © Dániel Horváth <horvath.daniel@mail.bme.hu>
// 2009 © Sergei Dorofeenko <sega@users.berlios.de>
// This file contains a set of classes for modelling of viscoelastic
// particles with bending and twisting momements in contacts.

#pragma once

#include <core/Dispatching.hpp>
#include <core/InteractionLoop.hpp>
#include <pkg/common/ElastMat.hpp>
#include <pkg/common/MatchMaker.hpp>
#include <pkg/dem/DemXDofGeom.hpp>
#include <pkg/dem/FrictPhys.hpp>
#include <pkg/dem/ScGeom.hpp>

#ifdef YADE_SPH
#include <pkg/common/SPHEngine.hpp>
#endif
#ifdef YADE_DEFORM
#include <core/PartialEngine.hpp>
#endif

namespace yade { // Cannot have #include directive inside.

/* Viscoelastic model with bending and twisting moments */

/// Material
class ViElToMat : public FrictMat {
	public:
		virtual ~ViElToMat();
	YADE_CLASS_BASE_DOC_ATTRS_CTOR(ViElToMat,FrictMat,"Material for linear spring-dashpot viscoelastic contact model with bending and twisting moments modeled by springs and viscous dampings.",
		((Real,betan,0.0,,"Normal Damping Ratio. Fraction of the viscous damping coefficient (normal direction) equal to $\\frac{c_{n}}{C_{n,crit}}$."))
		((Real,betas,0.0,,"Shear Damping Ratio. Fraction of the viscous damping coefficient (shear direction) equal to $\\frac{c_{s}}{C_{s,crit}}$."))
		((Real,Betakr,0.25,,"Dimensionless rolling elastic stiffness coefficient. Default value from [Jiang2015]."))
		((Real,Betakt,0.5,,"Dimensionless twisting elastic stiffness coefficient. Default value from [Jiang2015]."))
		((Real,Betacr,0.25,,"Dimensionless rolling viscous constant coefficient. Default value from [Jiang2015]."))
		((Real,Betact,0.5,,"Dimensionless twisting viscous constant coefficient. Default value from [Jiang2015]."))
		((Real,muRoll,-1,,"Rolling resistance coefficient. If negative, bending moment will be elastic."))
		((Real,muTwist,-1,,"Twisting resistance coefficient. If negative, twisting moment will be elastic."))
#ifdef YADE_SPH
		((bool,SPHmode,false,,"True, if SPH-mode is enabled."))
		((Real,mu,-1,, "Viscosity. See Mueller [Morris1997]_ ."))                                              // [Mueller2003], (14)
		((Real,h,-1,,  "Core radius. See Mueller [Mueller2003]_ ."))                                            // [Mueller2003], (1)
		((int,KernFunctionPressure,Lucy,, "Kernel function for pressure calculation (by default - Lucy). The following kernel functions are available: Lucy=1."))
		((int,KernFunctionVisco,   Lucy,, "Kernel function for viscosity calculation (by default - Lucy). The following kernel functions are available: Lucy=1."))
#endif

#ifdef YADE_DEFORM
		((bool,DeformEnabled,false,,"True, if particle deformation is needed. Off by default, see [Haustein2017]_ ."))
#endif
		,
		createIndex();
	);
	REGISTER_CLASS_INDEX(ViElToMat,FrictMat);
};
REGISTER_SERIALIZABLE(ViElToMat);

/// Interaction physics
class ViElToPhys : public FrictPhys{
	public:
		virtual ~ViElToPhys();
		Real R;
	YADE_CLASS_BASE_DOC_ATTRS_CTOR(ViElToPhys,FrictPhys,"IPhys created from :yref:`ViElToMat`, for use with :yref:`Law2_ScGeom_ViElToPhys_Basic`.",
		((Real,betan,0.0,,"Normal Damping Ratio of the contact"))
		((Real,betas,0.0,,"Shear Damping Ratio of the contact"))
		((Real,cn,0.0,,"Normal viscous constant of the contact"))
		((Real,cs,0.0,,"Shear viscous constant of the contact"))
		((Real,Fn,0.0,,"Normal force of the contact"))
		((Real,Fv,0.0,,"Viscous force of the contact"))
		((Real,kr,0.0,,"Rolling elastic stiffness of the contact (N m/rad)"))
		((Real,kt,0.0,,"Twisting elastic stiffness of the contact (N m/rad)"))
		((Real,cr,0.0,,"Rolling viscous constant of the contact"))
		((Real,ct,0.0,,"Twisting viscous constant of the contact"))
		((Real,maxRoll,0.0,,"Coefficient of rolling friction (negative means elastic)."))
		((Real,maxTwist,0.0,,"Coefficient of twisting friction (negative means elastic)."))
		// internal attributes
		((Vector3r,M_twist,Vector3r(0,0,0),,"Torsion/Twisting moment (N m)"))
		((Vector3r,M_bend,Vector3r(0,0,0),,"Bending/Rolling moment (N m)"))


#ifdef YADE_SPH
		((bool,SPHmode,false,,"True, if SPH-mode is enabled."))
		((Real,h,-1,,    "Core radius. See Mueller [Mueller2003]_ ."))                                            // [Mueller2003], (1)
		((Real,mu,-1,,   "Viscosity. See Mueller [Mueller2003]_ ."))                                              // [Mueller2003], (14)
#endif
#ifdef YADE_DEFORM
		((bool,DeformEnabled,false,,"True, if particle deformation mechanism is needed, see [Haustein2017]_ ."))
#endif
		,
		createIndex();
	)
#ifdef YADE_SPH
		KernelFunction kernelFunctionCurrentPressure;
		KernelFunction kernelFunctionCurrentVisco;
#endif
	REGISTER_CLASS_INDEX(ViElToPhys,FrictPhys);
};
REGISTER_SERIALIZABLE(ViElToPhys);

/// Convert material to interaction physics.
// Uses the rule of consecutively connection.
class Ip2_ViElToMat_ViElToMat_ViElToPhys: public IPhysFunctor {
	public :
		void        go(const shared_ptr<Material>& b1,
					const shared_ptr<Material>& b2,
					const shared_ptr<Interaction>& interaction) override;
	YADE_CLASS_BASE_DOC_ATTRS(Ip2_ViElToMat_ViElToMat_ViElToPhys,IPhysFunctor,"Convert 2 instances of :yref:`ViElToMat` to :yref:`ViElToPhys` using the rule of consecutive connection.",
		((shared_ptr<MatchMaker>,frictAngle,,,"Instance of :yref:`MatchMaker` determining how to compute interaction's friction angle. If ``None``, minimum value is used."))
		);
	virtual void Calculate_ViElToMat_ViElToMat_ViElToPhys(const shared_ptr<Material>& b1, const shared_ptr<Material>& b2, const shared_ptr<Interaction>& interaction, shared_ptr<ViElToPhys> phys);
	FUNCTOR2D(ViElToMat,ViElToMat);
};
REGISTER_SERIALIZABLE(Ip2_ViElToMat_ViElToMat_ViElToPhys);

/// Constitutive law
/// This class provides linear viscoelastic contact model with bending and twisting moments
class Law2_ScGeom_ViElToPhys_Basic: public LawFunctor {
	public :
		bool go(shared_ptr<IGeom>&, shared_ptr<IPhys>&, Interaction*) override;
	public :
	FUNCTOR2D(ScGeom,ViElToPhys);
	YADE_CLASS_BASE_DOC(Law2_ScGeom_ViElToPhys_Basic,LawFunctor,"Linear viscoelastic model operating on ScGeom and ViElToPhys. "
      "The contact law is visco-elastic in the normal direction, and visco-elastic frictional in the tangential direction. "
      "The normal contact is modelled as a spring of equivalent stiffness $k_n$, placed in parallel with a viscous damper "
      "of equivalent viscosity $c_n$. As for the tangential contact, it is made of a linear spring-dashpot system (in parallel "
      "with equivalent stiffness $k_s$ and viscosity $c_s$) in serie with a slider of friction coefficient "
      "$\\mu  = \\tan \\phi$.\n\nThe friction coefficient $\\mu  = \\tan \\phi$ is always evaluated as "
      "$\\tan(\\min(\\phi_1,\\phi_2))$, where $\\phi_1$ and $\\phi_2$ are respectively the friction angle of particle 1 "
      "and 2. Bending (rolling) and torsion (twisting) moments also considered and modeled as a torsion spring placed parallel with"
      "a viscous damper in both direction. For more information see []_ ."
	);
	DECLARE_LOGGER;
};
REGISTER_SERIALIZABLE(Law2_ScGeom_ViElToPhys_Basic);

Real contactParamCalc(const Real& l1,const Real& l2);
bool computeForceTorqueViElTo(shared_ptr<IGeom>& _geom, shared_ptr<IPhys>& _phys, Interaction* I, Vector3r & force, Vector3r & torque1, Vector3r & torque2);




#ifdef YADE_DEFORM
class DeformControl: public PartialEngine{
	public:
		virtual void action();
	YADE_CLASS_BASE_DOC_ATTRS_CTOR_PY(DeformControl,PartialEngine,"This engine implements particle deformation with const. volume, see [Haustein2017]_ . ",
		// Attrs
		,/* ctor */
		,/* py */
  );
};

REGISTER_SERIALIZABLE(DeformControl);
#endif

} // namespace yade

