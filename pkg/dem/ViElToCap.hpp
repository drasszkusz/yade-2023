#pragma once
#include "ViElTo.hpp"
#include <core/PartialEngine.hpp>
#include <boost/unordered_map.hpp>
#include <functional>

namespace yade { // Cannot have #include directive inside.


class ViElToCapMat : public ViElToMat {
	public:
		virtual ~ViElToCapMat();
	YADE_CLASS_BASE_DOC_ATTRS_CTOR(ViElToCapMat,ViElToMat,"Material for extended viscoelastic model of contact with capillary parameters.",
		((bool,Capillar,false,,"True, if capillar forces need to be added."))
#ifdef YADE_LIQMIGRATION
		((bool,LiqMigrEnabled,true,,"True, if liquid migration mechanism is needed. On by default."))
#endif
		((Real,Vb,0.0,,"Liquid bridge volume [m^3]"))
		((Real,gamma,0.0,,"Surface tension [N/m]"))
		((Real,theta,0.0,,"Contact angle [°]"))
		((Real,dcap,0.0,,"Damping coefficient for the capillary phase [-]"))
		((Real,muvisc,1.005e-3,,"Used only if rupt_type=2 or CapillarType is Lievano2017 or viscous liquid forces are active (Ca>=0.0)! Dynamic viscosity of liquid [Pa s]"))
		((Real,liq_dens,1000,,"Used only if rupt_type=3! Density of the liquid [kg/m^3]"))
		((std::string,CapillarType,"",,"Different types of capillar interaction: Willett_numeric, Willett_analytic [Willett2000]_ , Weigert [Weigert1999]_ (rupt_type=2 should not be used in this case), Rabinovich [Rabinov2005]_ , Lambert (simplified, corrected Rabinovich model) [Lambert2008]_ , Soulie [Soulie2006]_ , Pitois [Pitois2001]_ , Richefeu [Richefeu2008]_ , Mikami [Mikami1998]_ (Only sphere-sphere equations implemented), Sun [Sun2018]_ ")),
		createIndex();
	);
	REGISTER_CLASS_INDEX(ViElToCapMat,ViElToMat);
};
REGISTER_SERIALIZABLE(ViElToCapMat);

/// Interaction physics
enum CapType {None_Capillar, Willett_numeric, Willett_analytic, Weigert, Rabinovich, Lambert, Soulie, Pitois, Richefeu, Mikami, Sun};
class ViElToCapPhys : public ViElToPhys{
	typedef Real (* CapillarFunction)(const ScGeom& geom, ViElToCapPhys& phys);
	public:
		virtual ~ViElToCapPhys();
		Real R;
	YADE_CLASS_BASE_DOC_ATTRS_CTOR(ViElToCapPhys,ViElToPhys,"IPhys created from :yref:`ViElToCapMat`, for use with :yref:`Law2_ScGeom_ViElToCapPhys_Basic`.",
		((bool,Capillar,false,,"True, if capillar forces need to be added."))
		((bool,liqBridgeCreated,false,,"Whether liquid bridge was created, only after a normal contact of spheres"))
		((bool,liqBridgeActive,false,, "Whether liquid bridge is active at the moment"))
		((Real,sCrit,false,,"Critical bridge length [m]"))
		((Real,Vb,0.0,,"Liquid bridge volume [m^3]"))
		((Real,VL1,0.0,,"Liquid bridge volume contributed by particle 1 [m^3]"))
		((Real,VL2,0.0,,"Liquid bridge volume contributed bí particle 2 [m^3]"))
		((Real,gamma,0.0,,"Surface tension [N/m]"))
		((Real,theta,0.0,,"Effective contact angle [rad]"))
		((Real,theta1,0.0,,"Contact angle of particle 1 [rad]"))
		((Real,theta2,0.0,,"Contact angle of particle 2 [rad]"))
		((CapType,CapillarType,None_Capillar,,"Different types of capillar interaction: Willett_numeric, Willett_analytic, Weigert, Rabinovich, Lambert, Soulie, Pitois, Richefeu, Mikami, Sun"))
		((Real,dcap,0.0,,"Damping coefficient for the capillary phase [-]"))
		((Real,muvisc,0.0,,"Used only if rupt_type=2! Dynamic viscosity of liquid [Pa s]"))
		((Real,vrel,0.0,,"Relative velocity of the particles in contact [m/s]"))
		((Real,dens,0.0,,"density difference between the solid and the liquid phases [kg/m^3]"))
		((Real,R1,0.0,,"Radius of particle 1 in contact [m] (Only if CapillarType=Richefeu or Sun)"))
		((Real,R2,0.0,,"Radius of particle 2 in contact [m] (Only if CapillarType=Richefeu or Sun)"))
#ifdef YADE_LIQMIGRATION
		((bool,LiqMigrEnabled,,,"True, if liquid migration mechanism is needed."))
		((Real,Vmax,0.0,,"Maximal liquid bridge volume [m^3]"))
		((Real,Vf1,0.0,, "Liquid which will be returned to the 1st body after rupture [m^3]"))
		((Real,Vf2,0.0,, "Liquid which will be returned to the 2nd body after rupture [m^3]"))
#endif
		,
		createIndex();
	)
	REGISTER_CLASS_INDEX(ViElToCapPhys,ViElToPhys);
};
REGISTER_SERIALIZABLE(ViElToCapPhys);

/// Convert material to interaction physics.
class Ip2_ViElToCapMat_ViElToCapMat_ViElToCapPhys: public Ip2_ViElToMat_ViElToMat_ViElToPhys {
	public :
		void go(const shared_ptr<Material>& b1,
					const shared_ptr<Material>& b2,
					const shared_ptr<Interaction>& interaction) override;
	YADE_CLASS_BASE_DOC(Ip2_ViElToCapMat_ViElToCapMat_ViElToCapPhys,Ip2_ViElToMat_ViElToMat_ViElToPhys,"Convert 2 instances of :yref:`ViElToCapMat` to :yref:`ViElToCapPhys` using the rule of consecutive connection.");
	FUNCTOR2D(ViElToCapMat,ViElToCapMat);
};
REGISTER_SERIALIZABLE(Ip2_ViElToCapMat_ViElToCapMat_ViElToCapPhys);

/// Constitutive law
class Law2_ScGeom_ViElToCapPhys_Basic: public LawFunctor {
	public :
		bool go(shared_ptr<IGeom>&, shared_ptr<IPhys>&, Interaction*) override;
		static Real Willett_numeric_f     (const ScGeom& geom, ViElToCapPhys& phys);
		static Real Willett_analytic_f    (const ScGeom& geom, ViElToCapPhys& phys);
		static Real Weigert_f             (const ScGeom& geom, ViElToCapPhys& phys);
		static Real Rabinovich_f          (const ScGeom& geom, ViElToCapPhys& phys);
		static Real Lambert_f             (const ScGeom& geom, ViElToCapPhys& phys);
		static Real Soulie_f              (const ScGeom& geom, ViElToCapPhys& phys);
		static Real Pitois_f              (const ScGeom& geom, ViElToCapPhys& phys);
		static Real Richefeu_f            (const ScGeom& geom, ViElToCapPhys& phys);
		static Real Mikami_f              (const ScGeom& geom, ViElToCapPhys& phys);
		static Real Sun_f         	  (const ScGeom& geom, ViElToCapPhys& phys);
		static Real None_f                (const ScGeom& geom, ViElToCapPhys& phys);
		std::deque<std::function<Real(const ScGeom& geom, ViElToCapPhys& phys)> > CapFunctionsPool;
		Real critDist(const Real& Vb, const Real& R, const Real& Theta, const Real& Gamma, const Real& Muvisc, const Real& vrel, const Real& dens);
	FUNCTOR2D(ScGeom,ViElToCapPhys);
	YADE_CLASS_BASE_DOC_ATTRS_CTOR_PY(Law2_ScGeom_ViElToCapPhys_Basic,LawFunctor,"Extended version of Linear viscoelastic model with capillary parameters.",
		((Real,rupt_type,1,,"critical distance eq. type: 1 for [Willet2000]  eq., 2 for [Pitois2001] (Weigert1999 should not be used in this case), 3 for [Adams2002] and 4 for [Mikami1998] "))
		((Real,Ca,0.001,,"Capillary number for enable liquid viscosity forces to be taken into account in normal and tangential direction [Lian1998]_ . If >0.0 then viscous liquid forces are active if Ca is greater than this user given value. If =0.0 then viscous liquid forces are always calculated. If negative then viscous forces are not calculated. Default value is from [Tamrakar2019]_ ."))
		((OpenMPAccumulator<Real>,VLiqBridg,,Attr::noSave,"The total volume of liquid bridges"))
		((OpenMPAccumulator<int>, NLiqBridg,,Attr::noSave,"The total number of liquid bridges"))
		,{
//enum CapType {None_Capillar, Willett_numeric, Willett_analytic, Weigert, Rabinovich, Lambert, Soulie, Pitois, Richefeu, Mikami, Sun};
			CapFunctionsPool.resize(20, nullptr);
			CapFunctionsPool[None_Capillar] = None_f;
			CapFunctionsPool[Willett_numeric] = Willett_numeric_f;
			CapFunctionsPool[Willett_analytic] = Willett_analytic_f;
			CapFunctionsPool[Weigert] = Weigert_f;
			CapFunctionsPool[Rabinovich] = Rabinovich_f;
			CapFunctionsPool[Lambert] = Lambert_f;
			CapFunctionsPool[Soulie] = Soulie_f;
			CapFunctionsPool[Pitois] = Pitois_f;
			CapFunctionsPool[Richefeu] = Richefeu_f;
			CapFunctionsPool[Mikami] = Mikami_f;
			CapFunctionsPool[Sun] = Sun_f;
		 }
		,/* py */
		;
	)
	DECLARE_LOGGER;
};
REGISTER_SERIALIZABLE(Law2_ScGeom_ViElToCapPhys_Basic);

Real calc_func(const Real& R1, const Real& R2, const Real& Vb, const Real& s, const Real& Theta1, const Real& Theta2, const Real& alpha1);
Real calc_alpha2(const Real& R1, const Real& R2, const Real& s, const Real& Theta1, const Real& Theta2, const Real& alpha1);

#ifdef YADE_LIQMIGRATION
typedef boost::unordered_map<Body::id_t, int> mapBodyInt;
typedef boost::unordered_map<Body::id_t, Real> mapBodyReal;
class LiqControl: public PartialEngine{
	public:
		virtual void action();
		void addBodyMapInt( mapBodyInt & m, Body::id_t b );
		void addBodyMapReal( mapBodyReal & m, Body::id_t b, Real addV );
		Real vMax(shared_ptr<Body> b1, shared_ptr<Body> b2);
		Real totalLiqVol(int mask) const;
		Real liqVolBody(id_t id) const;
		bool addLiqInter(id_t id1, id_t id2, Real liq);
		void updateLiquid(shared_ptr<Body> b);
	YADE_CLASS_BASE_DOC_ATTRS_CTOR_PY(LiqControl,PartialEngine,"This engine implements liquid migration model, introduced here [Mani2013]_ . ",
		((int,mask,0,, "Bitmask for liquid  creation."))
		((Real,liqVolRup,0.,, "Liquid volume (integral value), which has been freed after rupture occured, [m^3]."))
		((Real,liqVolShr,0.,, "Liquid volume (integral value), which has been shared among of contacts, [m^3]."))
		((Real,vMaxCoef,0.03,, "Coefficient for vMax, [-]."))
		((bool,particleconserve,false,, "If True, the particle will have the same liquid volume during simulation e.g. liquid will not migrate [false]."))
		,/* ctor */
		,/* py */
		.def("totalLiq",&LiqControl::totalLiqVol,(boost::python::arg("mask")=0),"Return total volume of water in simulation.")
		.def("liqBody",&LiqControl::liqVolBody,(boost::python::arg("id")=-1),"Return total volume of water in body.")
		.def("addLiqInter",&LiqControl::addLiqInter,(boost::python::arg("id1")=-1, boost::python::arg("id2")=-1, boost::python::arg("liq")=-1),"Add liquid into the interaction.")
  );
};

Real liqVolIterBody (shared_ptr<Body> b);
REGISTER_SERIALIZABLE(LiqControl);
#endif

} // namespace yade

