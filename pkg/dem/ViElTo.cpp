// 2009 © Sergei Dorofeenko <sega@users.berlios.de>
#include "ViElTo.hpp"
#include <lib/high-precision/Constants.hpp>
#include <core/Omega.hpp>
#include <core/Scene.hpp>
#include <core/State.hpp>
#include <pkg/common/Sphere.hpp>
#include <pkg/dem/ScGeom.hpp>

#ifdef YADE_SPH
#include <pkg/common/SPHEngine.hpp>
#endif

#ifdef YADE_DEFORM
#include <boost/math/tools/roots.hpp>
#endif

namespace yade { // Cannot have #include directive inside.

using math::isfinite;

YADE_PLUGIN((ViElToMat)(ViElToPhys)(Ip2_ViElToMat_ViElToMat_ViElToPhys)(Law2_ScGeom_ViElToPhys_Basic));

/* ViElToMat */
ViElToMat::~ViElToMat(){}

/* ViElToPhys */
ViElToPhys::~ViElToPhys(){}


/* Ip2_ViElToMat_ViElToMat_ViElToPhys */
void Ip2_ViElToMat_ViElToMat_ViElToPhys::go(const shared_ptr<Material>& b1, const shared_ptr<Material>& b2, const shared_ptr<Interaction>& interaction) {
	// no updates of an existing contact 
	if(interaction->phys) return;
	shared_ptr<ViElToPhys> phys (new ViElToPhys());
	Calculate_ViElToMat_ViElToMat_ViElToPhys(b1, b2, interaction, phys);

#ifdef YADE_DEFORM
	const ViElToMat* mat1 = static_cast<ViElToMat*>(b1.get());
	const ViElToMat* mat2 = static_cast<ViElToMat*>(b2.get());
	phys->DeformEnabled = mat1->DeformEnabled && mat2->DeformEnabled;
#endif
	interaction->phys = phys;
}

/* Law2_ScGeom_ViElToPhys_Basic */
bool Law2_ScGeom_ViElToPhys_Basic::go(shared_ptr<IGeom>& _geom, shared_ptr<IPhys>& _phys, Interaction* I) {
	Vector3r force = Vector3r::Zero();
	Vector3r torque1 = Vector3r::Zero();
	Vector3r torque2 = Vector3r::Zero();
	if (computeForceTorqueViElTo(_geom, _phys, I, force, torque1, torque2) and (I->isActive)) {
		const int id1 = I->getId1();
		const int id2 = I->getId2();
		
		addForce (id1,-force,scene);
		addForce (id2, force,scene);
		addTorque(id1, torque1,scene);
		addTorque(id2, torque2,scene);
		return true;
	} else return false;
}

bool computeForceTorqueViElTo(shared_ptr<IGeom>& _geom, shared_ptr<IPhys>& _phys, Interaction* I, Vector3r & force, Vector3r & torque1, Vector3r & torque2) {
	ViElToPhys& phys=*static_cast<ViElToPhys*>(_phys.get());
	const ScGeom& geom=*static_cast<ScGeom*>(_geom.get());
	Scene* scene=Omega::instance().getScene().get();

#ifdef YADE_SPH
//=======================================================================================================
	if (phys.SPHmode) {
		if (computeForceSPH(_geom, _phys, I, force)) {
			return true;
		} else {
			return false;
		}
	}
//=======================================================================================================
#endif

	const int id1 = I->getId1();
	const int id2 = I->getId2();


	Real addDR = 0.;
#ifdef YADE_DEFORM
	const BodyContainer& bodies = *scene->bodies;
	const State& de1 = *static_cast<State*>(bodies[id1]->state.get());
	const State& de2 = *static_cast<State*>(bodies[id2]->state.get());
	addDR = de1.dR + de2.dR;
#endif

	if ((geom.penetrationDepth + addDR)<0) {
		return false;
	} else {
#ifndef YADE_DEFORM
		// These 3 lines were duplicated (see above) not to loose
		// runtime performance, if YADE_DEFORM is disabled and no
		// contact detected
		const BodyContainer& bodies = *scene->bodies;
		const State& de1 = *static_cast<State*>(bodies[id1]->state.get());
		const State& de2 = *static_cast<State*>(bodies[id2]->state.get());
#endif
		Vector3r& shearForce = phys.shearForce;
		if (I->isFresh(scene)) shearForce=Vector3r(0,0,0);
		const Real& dt = scene->dt;
		shearForce = geom.rotate(shearForce);
	
		// Handle periodicity.
		const Vector3r shift2 = scene->isPeriodic ? scene->cell->intrShiftPos(I->cellDist): Vector3r::Zero(); 
		const Vector3r shiftVel = scene->isPeriodic ? scene->cell->intrShiftVel(I->cellDist): Vector3r::Zero(); 
	
		const Vector3r c1x = (geom.contactPoint - de1.pos);
		const Vector3r c2x = (geom.contactPoint - de2.pos - shift2);
		
		const Vector3r relativeVelocity = (de1.vel+de1.angVel.cross(c1x)) - (de2.vel+de2.angVel.cross(c2x)) + shiftVel;
		const Real normalVelocity	= geom.normal.dot(relativeVelocity);
		const Vector3r shearVelocity	= relativeVelocity-normalVelocity*geom.normal;

		// As Chiara Modenese suggest, we store the elastic part 
		// and then add the viscous part if we pass the Mohr-Coulomb criterion.
		// See http://www.mail-archive.com/yade-users@lists.launchpad.net/msg01391.html
		shearForce += phys.ks*dt*shearVelocity; // the elastic shear force have a history, but
		Vector3r shearForceVisc = Vector3r::Zero(); // the viscous shear damping haven't a history because it is a function of the instant velocity 
	
	
		// Prevent appearing of attraction forces due to a viscous component
		// [Radjai2011], page 3, equation [1.7]
		// [Schwager2007]
		phys.Fn = phys.kn * (geom.penetrationDepth + addDR);
		phys.Fv = phys.cn * normalVelocity;
		const Real normForceReal = phys.Fn + phys.Fv;
		if (normForceReal < 0) {
			phys.normalForce = Vector3r::Zero();
		} else {
			phys.normalForce = normForceReal * geom.normal;
		}
		
		// Moment Law, see Jiang 2015 //
		//* Angular velocities *//
		const Vector3r relAngVel  = de1.angVel - de2.angVel; //Relative angular velocity
		Vector3r relAngVelTwist = geom.normal.dot(relAngVel)*geom.normal; //Twisting part of the relative angular velocity
		Vector3r relAngVelBend = relAngVel - relAngVelTwist; // Bending part of the relative angular velocity
		
		//* Bending moment *//
		Vector3r& momentBend = phys.M_bend;
		momentBend = geom.rotate(momentBend); // rotate bending moment vector
		Vector3r momentBendElast = phys.kr*relAngVelBend*dt; // Elastic part of the bending moment
		Vector3r momentBendVisc = phys.cr*relAngVelBend; //Viscous part of the bending moment
		momentBend = momentBend-momentBendElast; // Total bending moment
		
		//* Torsion moment *//
		Vector3r& momentTwist = phys.M_twist;
		momentTwist = geom.rotate(momentTwist); // rotate torsion moment vector
		Vector3r momentTwistElast = phys.kt*relAngVelTwist*dt; // Elastic part of the torsion moment
		Vector3r momentTwistVisc = phys.ct*relAngVelTwist; //Viscous part of the torsion moment
		momentTwist = momentTwist-momentTwistElast; // Total torsion moment
		
		/// Plasticity ///
		// limit rolling moment to the plastic value, if required
		if (phys.maxRoll>=0.){ // do we want to apply plasticity?
			Real RollMax = phys.maxRoll*phys.normalForce.norm();
			Real scalarRoll = phys.M_bend.norm();
			if (scalarRoll>RollMax){ // fix maximum rolling moment
				Real ratio = RollMax/scalarRoll;
				phys.M_bend *= ratio;
			}
			else 
			{
				phys.M_bend+=-momentBendVisc;
			}
		}
		
		// limit torsion moment to the plastic value, if required
		if (phys.maxTwist>=0.){ // do we want to apply plasticity?
			Real TwistMax = phys.maxTwist*phys.normalForce.norm()*phys.tangensOfFrictionAngle;
			Real scalarTwist = phys.M_twist.norm();
			if (scalarTwist>TwistMax){ // fix maximum torsion moment
				Real ratio = TwistMax/scalarTwist;
				phys.M_twist *= ratio;
			}
			else 
			{
				phys.M_twist+=-momentTwistVisc;
			}
		}
		
		
		const Real maxFs = phys.normalForce.squaredNorm() * std::pow(phys.tangensOfFrictionAngle,2);
		if( shearForce.squaredNorm() > maxFs )
		{
			// Then Mohr-Coulomb is violated (so, we slip), 
			// we have the max value of the shear force, so 
			// we consider only friction damping.
			const Real ratio = sqrt(maxFs) / shearForce.norm();
			shearForce *= ratio;
		} 
		else 
		{
			// Then no slip occurs we consider friction damping + viscous damping.
			shearForceVisc = phys.cs*shearVelocity; 
		}
		force = phys.normalForce + shearForce + shearForceVisc;
		torque1 = -c1x.cross(force)+momentBend+momentTwist;
		torque2 = c2x.cross(force)-momentBend-momentTwist;
		return true;
	}
}

void Ip2_ViElToMat_ViElToMat_ViElToPhys::Calculate_ViElToMat_ViElToMat_ViElToPhys(const shared_ptr<Material>& b1, const shared_ptr<Material>& b2, const shared_ptr<Interaction>& interaction, shared_ptr<ViElToPhys> phys) {
	ViElToMat* mat1 = static_cast<ViElToMat*>(b1.get());
	ViElToMat* mat2 = static_cast<ViElToMat*>(b2.get());
	
	GenericSpheresContact* sphCont=YADE_CAST<GenericSpheresContact*>(interaction->geom.get());
	Real R1=sphCont->refR1>0?sphCont->refR1:sphCont->refR2;
	Real R2=sphCont->refR2>0?sphCont->refR2:sphCont->refR1;
	Real R=2*(R1*R2) /(R1+R2);
	
	Real kn1 = 0.0; Real kn2 = 0.0;
	Real ks1 = 0.0; Real ks2 = 0.0;

	Real betan1 = 0.0; Real betan2 = 0.0;
	Real betas1 = 0.0; Real betas2 = 0.0;
	
    //Set parameters on the base of young modulus
	kn1 = 2*mat1->young*R1;
	kn2 = 2*mat2->young*R2;
	ks1 = kn1*mat1->poisson;
	ks2 = kn2*mat2->poisson;

	betan1 = mat1->betan;
	betan2 = mat2->betan;
	betas1 = mat1->betas;
	betas2 = mat2->betas;
	
	phys->kn = contactParamCalc(kn1,kn2);
	phys->ks = contactParamCalc(ks1,ks2);

	Body::id_t id1 = interaction->getId1(); // get id body 1
	Body::id_t id2 = interaction->getId2(); // get id body 2
	State* de1 = Body::byId(id1,scene)->state.get();
	State* de2 = Body::byId(id2,scene)->state.get();
	const shared_ptr<Body>& ba=Body::byId(id1,scene); 
	const shared_ptr<Body>& bb=Body::byId(id2,scene);
	Real mbar = (!ba->isDynamic() && bb->isDynamic()) ? de2->mass : ((!bb->isDynamic() && ba->isDynamic()) ? de1->mass : (de1->mass*de2->mass / (de1->mass + de2->mass))); // get equivalent mass if both bodies are dynamic, if not set it equal to the one of the dynamic body

	if (betan1!=0 || betan2!=0){
		Real cn_crit = 2.*sqrt(mbar*phys->kn); // Critical damping coefficient (normal direction)
		phys->cn = cn_crit * ( (betan1!=0 && betan2!=0) ? ((betan1+betan2)/2.) : ( (betan2==0) ? betan1/2. : betan2/2. )); // Damping normal coefficient
	}
	else {
		phys->cn=0.;
	}

	if (betas1!=0 || betas2!=0){
		Real cs_crit = 2.*sqrt(mbar*phys->ks); // Critical damping coefficient (normal direction)
		phys->cs = cs_crit * ( (betas1!=0 && betas2!=0) ? ((betas1+betas2)/2.) : ( (betas2==0) ? betas1/2. : betas2/2. )); // Damping tangential coefficient
	}
	else {
		phys->cs=0.;
	}

	Real Betakr, Betakt, Betacr, Betact;
	Betakr = contactParamCalc(mat1->Betakr,mat2->Betakr);
	Betakt = contactParamCalc(mat1->Betakt,mat2->Betakt);
	Betacr = contactParamCalc(mat1->Betacr,mat2->Betacr);
	Betact = contactParamCalc(mat1->Betact,mat2->Betact);

	phys->kr = Betakr*phys->kn*R1*R2;
	phys->kt = Betakt*phys->ks*R1*R2;
	phys->cr = Betacr*phys->cn*R1*R2;
	phys->ct = Betact*phys->cs*R1*R2;
	Real MuRoll, MuTwist;
	MuRoll = std::min(mat1->muRoll,mat2->muRoll);
	MuTwist = std::min(mat1->muTwist,mat2->muTwist);
	phys->maxRoll = MuRoll*R;
	phys->maxTwist = MuTwist*R;
	Real fa = mat1->frictionAngle;
	Real fb = mat2->frictionAngle;

	Real frictionAngle = (!frictAngle) ? std::min(fa,fb) : (*frictAngle)(mat1->id,mat2->id,fa,fb);
	phys->tangensOfFrictionAngle = std::tan(frictionAngle);

	phys->shearForce = Vector3r(0,0,0);

#ifdef YADE_SPH
	if (mat1->SPHmode and mat2->SPHmode)  {
		phys->SPHmode=true;
		phys->mu=(mat1->mu+mat2->mu);
		phys->h=(mat1->h+mat2->h)/2.0;
	}
	
	phys->kernelFunctionCurrentPressure = returnKernelFunction (mat1->KernFunctionPressure, mat2->KernFunctionPressure, Grad);
	phys->kernelFunctionCurrentVisco    = returnKernelFunction (mat1->KernFunctionVisco, mat2->KernFunctionVisco, Lapl);
#endif
}

/* Contact parameter calculation function */
Real contactParamCalc(const Real& l1, const Real& l2){
  // If one of paramaters > 0. we DO NOT return 0
  Real a = 2.0*l1*l2/(l1+l2);
  if (l1>0 && l2>0) return a;
  else return 0;
}

#ifdef YADE_DEFORM
// The reference paper [Haustein2017]
// functor with Raji1999 Eq. 2.52
template <class T>
struct fkt_functor
{
  fkt_functor(T Radius, T tdR, vector<T>& distanceVector) : R(Radius), dR(tdR), coef(distanceVector) {}

  pair<T, T> operator()(T const& Rs)
  {
    // solve for radius of deformed sphere Rs
    T funktion = -R*R*R + Rs*Rs*Rs;        // Raji1999 Eq. 2.52 - Part outside of the sum
    T dfunktion = 3*Rs*Rs;                 // Derivation of Raji1999 Eq. 2.52 - Part outside of the sum

    // Summation over every contact distance dsi (C++11)
    for(auto const& dsi : coef)
    {
      funktion  += -0.25 * (Rs - dsi)*(Rs - dsi) * (2 * Rs + dsi) ;      // Raji1999 Eq. 2.51 - part in the sum
      dfunktion += 3.0/2.0 * ( Rs*Rs - Rs*dsi ) ;                        // Derivation of Raji1999 Eq. 2.52 - part in the sum
    }

        return make_pair(funktion, dfunktion);
  }
  private:
    T R;                    // radius of the undeformed sphere
    T dR;                   // dR of sphere
    vector <T> coef;        // vector of all contact distances dsi
};

// function for easy calling of Newton-Raphson method
template <class T>
T fkt(T R, T dR, vector<T> z)
{
  double guess = R + dR;       // start guess
  double min = guess * 0.99;   // minimum
  double max = guess * 1.05 ;  // maximum
  int digits = std::numeric_limits<T>::digits ;

  // use Newton-Raphson method for numerical solution
  return boost::math::tools::newton_raphson_iterate(fkt_functor<T>(R, dR, z), guess, min, max, digits);
}


YADE_PLUGIN((DeformControl));
void DeformControl::action()
{
  Scene* scene=Omega::instance().getScene().get();
  const BodyContainer& b = *scene->bodies;

  for(int i = 0; i < b.size(); ++i)
  {
    vector<double> dsi;
    if ( Sphere* s1 = dynamic_cast<Sphere*>(b[i]->shape.get()) )
    {
      double s1Rad = s1->radius ;
      State* s1_state = static_cast<State*>(b[i]->state.get());
      double s1dR  = s1_state->dR ;

      for(Body::MapId2IntrT::iterator it=b[i]->intrs.begin(),end=b[i]->intrs.end(); it!=end; ++it)
      {
        if(!it->second->isReal()) continue;

        unsigned int partnerID;
        if( it->second->getId1() == i )
        {
          partnerID = it->second->getId2();
        } else
        {
          partnerID = it->second->getId1();
        }

        // Sphere - Sphere contact
        if(Sphere* s2 = dynamic_cast<Sphere*>(b[partnerID]->shape.get() ))
        {
          double s2Rad = s2->radius;
          State* s2_state = static_cast<State*>(b[partnerID]->state.get());
          double s2dR  = s2_state->dR;

          if ( ScGeom* scg = dynamic_cast<ScGeom*>(it->second->geom.get())  )
          {
            double L = s1Rad + s2Rad - scg->penetrationDepth;
            double s1RdR = s1Rad + s1dR;
            double s2RdR = s2Rad + s2dR;
            double ds = (L*L + s1RdR*s1RdR - s2RdR*s2RdR) / (2.0 * L);
            dsi.push_back( ds );
          }
        }
        else   // Sphere - Facet / Wall contact
        {
          if ( ScGeom* scg = dynamic_cast<ScGeom*>(it->second->geom.get())  )
          {
            double ds = s1Rad - scg->penetrationDepth ;
            dsi.push_back( ds );
          }
        }
      }
      s1_state->dR = fkt(s1Rad, s1dR, dsi) - s1Rad;
    }
  }
}
#endif

} // namespace yade
